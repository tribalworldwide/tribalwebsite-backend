﻿using Infrastructure.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class Contact : BaseEntity
    {
        [StringLength(1000)]
        public string Address { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(20)]
        public string Telephone { get; set; }

        [StringLength(20)]
        public string Fax { get; set; }

        [StringLength(250)]
        public string EJournal { get; set; }

        [StringLength(250)]
        public string ShareWithUs { get; set; }
    }
}
