﻿using Infrastructure.Data.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class AwardDetail : BaseEntity
    {
        public long? AwardId { get; set; }
        [ForeignKey("AwardId")]
        public virtual Award Award { get; set; }

        public long? WorkId { get; set; }
        [ForeignKey("WorkId")]
        public virtual Work Work { get; set; }

        [Required]
        public int Year { get; set; }

        [Required]
        [StringLength(750)]
        public string Category { get; set; }

        [Required]
        [StringLength(750)]
        public string Prize { get; set; }
    }
}
