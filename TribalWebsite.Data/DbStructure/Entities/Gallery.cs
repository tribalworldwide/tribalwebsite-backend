﻿using Infrastructure.Data.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class Gallery : BaseEntity
    {
        public long? WorkId { get; set; }
        [ForeignKey("WorkId")]
        public virtual Work Work { get; set; }

        public long? GalleryTypeId { get; set; }
        [ForeignKey("GalleryTypeId")]
        public virtual GalleryType GalleryType { get; set; }

        public ICollection<Media> Media { get; set; }

    }
}
