﻿using Infrastructure.Data.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class Award : BaseEntity
    {
        [Required]
        [StringLength(300)]
        public string Name { get; set; }

        public long? MediaId { get; set; }

        [ForeignKey("MediaId")]
        public virtual Media Media { get; set; }

       public ICollection<AwardDetail> AwardDetails { get; set; }
    }
}
