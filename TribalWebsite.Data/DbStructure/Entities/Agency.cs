﻿using Infrastructure.Data.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class Agency : BaseEntity
    {
        [Required]
        [StringLength(150)]
        public string Content { get; set; }

        [Required]
        public string History { get; set; }

        [Required]
        public string Idea { get; set; }

        public long MediaId { get; set; }

        [ForeignKey("MediaId")]
        public virtual Media Media { get; set; }

    }
}
