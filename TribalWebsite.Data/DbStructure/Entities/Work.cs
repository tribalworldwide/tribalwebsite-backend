﻿using Infrastructure.Data.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class Work : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string ShortDescription { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Brief { get; set; }
        [Required]
        public string Idea { get; set; }

        public string HtmlContent { get; set; }

        [Required]
        public bool IsDouble { get; set; }


        public long? ClientId { get; set; }
        [ForeignKey("ClientId")]
        public virtual Client Client { get; set; }

        public long? CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public long? ThumbnailId { get; set; }
        [ForeignKey("ThumbnailId")]
        public virtual Media Thumbnail { get; set; }

        public long? DoubleThumbnailId { get; set; }
        [ForeignKey("DoubleThumbnailId")]
        public virtual Media DoubleThumbnail { get; set; }

        public ICollection<AwardDetail> AwardDetail { get; set; }
        public ICollection<Gallery> Gallery { get; set; }

    }
}
