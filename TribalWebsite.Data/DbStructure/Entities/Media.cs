﻿using Infrastructure.Data.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class Media : BaseEntity
    {
        [Required]
        public int MediaType { get; set; }
        [Required]
        public string FilePath { get; set; }

        public long? GalleryId { get; set; }
        [ForeignKey("GalleryId")]
        public virtual Gallery Gallery { get; set; }
    }
}
