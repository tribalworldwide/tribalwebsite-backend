﻿using Infrastructure.Data.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class Client : BaseEntity
    {
        [Required]
        [StringLength(350)]
        public string Name { get; set; }

        public long? MediaId { get; set; }

        [ForeignKey("MediaId")]
        public virtual Media Media { get; set; }
    }
}
