﻿using System.ComponentModel.DataAnnotations;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class SocialMedia
    {
        [Key]
        public long Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Link { get; set; }

        [Required]
        public string Type { get; set; }
    }
}
