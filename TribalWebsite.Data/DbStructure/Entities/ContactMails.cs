﻿using System.ComponentModel.DataAnnotations;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class ContactMails
    {
        [Key]
        public long Id { get; set; }
        public string Email { get; set; }
    }
}
