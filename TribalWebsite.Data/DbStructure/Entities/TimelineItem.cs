﻿using Infrastructure.Data.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class TimelineItem : BaseEntity
    {
        public long? ItemId { get; set; }

        public long? TimelineTypeId { get; set; }

        [ForeignKey("TimelineTypeId")]
        public virtual TimelineType TimelineType { get; set; }
    }
}
