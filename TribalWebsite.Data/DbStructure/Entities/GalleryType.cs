﻿using System.ComponentModel.DataAnnotations;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class GalleryType
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }

    }
}