﻿using Infrastructure.Data.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class News : BaseEntity
    {
        [Required]
        [StringLength(350)]
        public string Title { get; set; }

        [Required]
        [StringLength(750)]
        public string Content { get; set; }

        [Required]
        [StringLength(100)]
        public string LinkTitle { get; set; }

        [Required]
        public string Link { get; set; }

        public DateTime? PublishDate { get; set; }
    }
}
