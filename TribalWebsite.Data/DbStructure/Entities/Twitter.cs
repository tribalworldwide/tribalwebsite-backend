﻿using Infrastructure.Data.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class Twitter : BaseEntity
    {
        [Required]
        [StringLength(300)]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public string User { get; set; }
        [Required]
        public string UserLink { get; set; }

        public DateTime? PostDate { get; set; }
    }
}
