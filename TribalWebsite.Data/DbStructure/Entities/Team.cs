﻿using Infrastructure.Data.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class Team : BaseEntity
    {
        [StringLength(100)]
        public string Fullname { get; set; }

        [Required]
        [StringLength(500)]
        public string Position { get; set; }

        [Required]
        public string Content { get; set; }


        public long MediaId { get; set; }

        [ForeignKey("MediaId")]
        public virtual Media Media { get; set; }
    }
}
