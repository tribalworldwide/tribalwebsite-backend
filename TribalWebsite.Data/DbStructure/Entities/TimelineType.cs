﻿using System.ComponentModel.DataAnnotations;

namespace TribalWebsite.Data.DbStructure.Entities
{
    public class TimelineType
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
