﻿using Infrastructure.Data.Entities;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Helpers;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Data.DbStructure.Context
{
    public class TribalWebsiteContext : DbContext
    {
        public TribalWebsiteContext() : base("TribalWebsiteConnection")
        {
            Database.SetInitializer(new DbInitializer());
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Language> Language { get; set; }
        public DbSet<Log> Log { get; set; }
        public DbSet<AdminUser> AdminUser { get; set; }

        public DbSet<Agency> Agency { get; set; }
        public DbSet<Award> Award { get; set; }
        public DbSet<AwardDetail> AwardDetail { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Contact> Contact { get; set; }
        public DbSet<ContactMails> ContactMails { get; set; }
        public DbSet<Gallery> Gallery { get; set; }
        public DbSet<GalleryType> GalleryType { get; set; }
        public DbSet<Media> Media { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<SocialMedia> SocialMedia { get; set; }
        public DbSet<Story> Story { get; set; }
        public DbSet<Team> Team { get; set; }
        public DbSet<TimelineItem> TimelineItem { get; set; }
        public DbSet<TimelineType> TimelineType { get; set; }
        public DbSet<Twitter> Twitter { get; set; }
        public DbSet<Work> Work { get; set; }

        private void PreInsertListener()
        {
            foreach (var entity in ChangeTracker.Entries<BaseEntity>().Where(x => x.State == EntityState.Added).ToList())
            {
                entity.Entity.CreatedAt = DateTime.Now;
                entity.Entity.State = (int)State.Active;
                if (entity.Entity.LanguageId == null)
                    entity.Entity.LanguageId = LanguageHelper.CurrentLanguage;
            }
        }

        private void UpdateListener()
        {
            foreach (var entity in ChangeTracker.Entries<BaseEntity>().Where(x => x.State == EntityState.Modified).ToList())
            {
                entity.Entity.UpdatedAt = DateTime.Now;
            }
        }

        public override int SaveChanges()
        {
            PreInsertListener();
            UpdateListener();
            return base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                .Where(type => !String.IsNullOrEmpty(type.Namespace))
                .Where(type => type.BaseType != null && type.BaseType.IsGenericType
                && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }
            base.OnModelCreating(modelBuilder);
        }
    }
}
