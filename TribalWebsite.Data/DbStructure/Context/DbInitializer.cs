﻿using Infrastructure.Data.Entities;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Helpers;
using System.Data.Entity;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Data.DbStructure.Context
{
    internal class DbInitializer : DropCreateDatabaseIfModelChanges<TribalWebsiteContext>
    {
        protected override void Seed(TribalWebsiteContext context)
        {
            context.Language.Add(new Language { Name = "Türkçe", Abbrevation = "TR" });

            context.Language.Add(new Language { Name = "English", Abbrevation = "EN" });

            context.Contact.Add(new Contact
            {
                Address = "Tuzambarı<br>Bedrettin Mah.<br>Havuzbaşı Değirmeni Sk.<br>No: 2 Kasımpaşa-Beyoğlu 34440",
                EJournal = "We hire people that have a passion for creating great work and the talent to make it look easy. If you think that describes you, drop us a line.",
                Telephone = "+90 212 311 46 46",
                ShareWithUs = "Folow us with our social media channels.<br>See how we enjoy our lives! Fun Fun Fun",
                LanguageId = 1,
                Email = "info@tribalistanbul.com",
                Fax = "+90 212 311 46 56"
            });

            var pass = new PasswordHash("q12345");

            context.AdminUser.Add(new AdminUser
            {
                Username = "admin",
                Password = pass.ToArray()
            });

            context.TimelineType.Add(new TimelineType
            {
                Name = "Work"
            });
            context.TimelineType.Add(new TimelineType
            {
                Name = "News"
            });
            context.TimelineType.Add(new TimelineType
            {
                Name = "Media"
            });
            context.TimelineType.Add(new TimelineType
            {
                Name = "Instagram"
            });
            context.TimelineType.Add(new TimelineType
            {
                Name = "Twitter"
            });

            context.Media.Add(new Media
            {
                FilePath = "/Uploads/demo-140-51.png",
                MediaType = (int)MediaType.Image
            });

            context.SaveChanges();

            context.Agency.Add(new Agency
            {
                Content = "İnsanlarla markalar arasında ilişkiler tasarlıyor, bağlar yaratıyoruz.",
                History = "Dünyanın en büyük reklam ajansı networklerinden biri olan DDB Grubu’na bağlı Tribal Worldwide’ın İstanbul şubesiyiz. 2013 yılında, Türkiye’nin en büyük dijital ajanslarından biriyken, Cannes Lions’ta dünya üçüncülüğüne kadar yükselmiş DDB&Co ile güç birliği yaptık.",
                Idea = "Türkiye İş Bankası, mobil bankacılık uygulaması İşCep için Cem Yılmaz ile birlikte hazırladığı O İş Cepte kampanyasının üçüncü reklam filmini yayınladı. İzleyicileri teknolojinin nimetlerinden yararlanarak yatırım hedeflerini büyütmeye çağıran Cem Yılmaz‘ın İşCep Mobil Borsa’yı tanıttığı animasyon gösterisi ilerleye dönemde yeni filmlerle devam edecek.",
                MediaId = 1
            });

            context.SaveChanges();

            base.Seed(context);
        }
    }
}