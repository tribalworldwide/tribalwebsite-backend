﻿using System.Web.Mvc;
using TribalWebsite.Api.PageInteractions.ContactInteractions;

namespace TribalWebsite.Web.Controllers
{
    public class ContactController : Controller
    {
        private IContactInteraction contactInteraction;

        public ContactController(IContactInteraction contactInteraction)
        {
            this.contactInteraction = contactInteraction;
        }

        [Route("contact")]
        public ActionResult Index()
        {
            return View(contactInteraction.GetContactPage());
        }
    }
}