﻿using System.Web.Mvc;
using TribalWebsite.Api.PageInteractions.AgencyInteractions;

namespace TribalWebsite.Web.Controllers
{
    public class AgencyController : Controller
    {
        private IAgencyInteraction agencyInteraction;

        public AgencyController(IAgencyInteraction agencyInteraction)
        {
            this.agencyInteraction = agencyInteraction;
        }

        [Route("agency")]
        public ActionResult Index()
        {
            return View(agencyInteraction.GetAgencyPage());
        }
    }
}