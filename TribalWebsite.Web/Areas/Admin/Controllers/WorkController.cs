﻿using System.Web;
using System.Web.Mvc;
using TribalWebsite.Api.Dto;
using TribalWebsite.Api.Interactions.CategoryInteractions;
using TribalWebsite.Api.Interactions.ClientInteractions;
using TribalWebsite.Api.Interactions.WorkInteractions;

namespace TribalWebsite.Web.Areas.Admin.Controllers
{
    public class WorkController : Controller
    {
        private IWorkInteraction workInteraction;
        private IClientInteraction clientInteraction;
        private ICategoryInteraction categoryInteraction;

        public WorkController(IWorkInteraction workInteraction, IClientInteraction clientInteraction, ICategoryInteraction categoryInteraction)
        {
            this.workInteraction = workInteraction;
            this.clientInteraction = clientInteraction;
            this.categoryInteraction = categoryInteraction;
        }
        
        [Authorize]
        public ActionResult Index()
        {
            var model = workInteraction.GetAllWorks();

            return View(model);
        }

        [Authorize]
        public ActionResult Create()
        {
            ViewBag.Clients = clientInteraction.GetAllClients();

            ViewBag.Categories = categoryInteraction.GetAllCategories();

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(WorkDto workDto, HttpPostedFileBase thumbFile, HttpPostedFileBase doubleImageFile)
        {
            if (thumbFile != null && thumbFile.ContentLength > 0)
                workInteraction.SaveWork(workDto, thumbFile, doubleImageFile);

            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Edit(long? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            ViewBag.Clients = clientInteraction.GetAllClients();

            ViewBag.Categories = categoryInteraction.GetAllCategories();

            var model = workInteraction.GetWorkById(id.Value);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(WorkDto workDto, HttpPostedFileBase thumbFile, HttpPostedFileBase doubleImageFile)
        {
            workInteraction.UpdateWork(workDto, thumbFile, doubleImageFile);
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult State(long? id)
        {
            workInteraction.SetState(id);
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult SetOrder(long? id, int order)
        {
            workInteraction.SetOrder(id, order);
            return RedirectToAction("Index");
        }
    }
}