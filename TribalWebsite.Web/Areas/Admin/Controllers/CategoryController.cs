﻿using System.Web.Mvc;
using TribalWebsite.Api.Dto;
using TribalWebsite.Api.Interactions.CategoryInteractions;

namespace TribalWebsite.Web.Areas.Admin.Controllers
{
    public class CategoryController : Controller
    {
        private ICategoryInteraction categoryInteraction;

        public CategoryController(ICategoryInteraction categoryInteraction)
        {
            this.categoryInteraction = categoryInteraction;
        }

        [Authorize]
        public ActionResult Index()
        {
            return View(categoryInteraction.GetAllCategories());
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(CategoryDto categoryDto)
        {
            categoryInteraction.SaveCategory(categoryDto);

            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Edit(long? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var model = categoryInteraction.GetCategoryById(id.Value);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(CategoryDto categoryDto)
        {
            categoryInteraction.UpdateCategory(categoryDto);
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult SetOrder(long? id, int order)
        {
            categoryInteraction.SetOrder(id, order);
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult State(long? id)
        {
            categoryInteraction.SetState(id);
            return RedirectToAction("Index");
        }
    }
}