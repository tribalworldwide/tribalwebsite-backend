﻿using System.Web;
using System.Web.Mvc;
using TribalWebsite.Api.Dto;
using TribalWebsite.Api.Interactions.AwardInteractions;

namespace TribalWebsite.Web.Areas.Admin.Controllers
{
    public class AwardsController : Controller
    {
        private IAwardInteraction awardInteraction;

        public AwardsController(IAwardInteraction awardInteraction)
        {
            this.awardInteraction = awardInteraction;
        }

        [Authorize]
        public ActionResult Index()
        {
            return View(awardInteraction.GetAllAwards());
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(AwardDto awardDto, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                awardInteraction.SaveAward(awardDto, file);

            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var model = awardInteraction.GetAwardById(id.Value);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(AwardDto awardDto, HttpPostedFileBase file)
        {
            awardInteraction.UpdateAward(awardDto, file);
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Delete(long? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var model = awardInteraction.GetAwardById(id.Value);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(AwardDto awardDto)
        {
            awardInteraction.DeleteAward(awardDto);
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult SetOrder(long? id, int order)
        {
            awardInteraction.SetOrder(id, order);
            return RedirectToAction("Index");
        }


    }
}