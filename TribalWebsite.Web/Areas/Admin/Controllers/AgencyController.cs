﻿using System.Web;
using System.Web.Mvc;
using TribalWebsite.Api.Dto;
using TribalWebsite.Api.Interactions.AgencyInteractions;

namespace TribalWebsite.Web.Areas.Admin.Controllers
{
    public class AgencyController : Controller
    {
        private IAgencyInteraction agencyInteraction;

        public AgencyController(IAgencyInteraction agencyInteraction)
        {
            this.agencyInteraction = agencyInteraction;
        }

        [Authorize]
        public ActionResult Index()
        {
            var model = agencyInteraction.GetAgency();

            return View(model);
        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var model = agencyInteraction.GetAgencyById(id.Value);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(AgencyDto agencyDto, HttpPostedFileBase file)
        {
            agencyInteraction.UpdateAgency(agencyDto, file);
            return RedirectToAction("Index");
        }
    }
}