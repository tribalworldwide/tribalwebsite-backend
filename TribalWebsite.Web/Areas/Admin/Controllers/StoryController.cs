﻿using Infrastructure.Data.Enums;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TribalWebsite.Api.Dto;
using TribalWebsite.Api.Interactions.StoryInteractions;

namespace TribalWebsite.Web.Areas.Admin.Controllers
{
    public class StoryController : Controller
    {
        private IStoryInteraction storyInteraction;

        public StoryController(IStoryInteraction storyInteraction)
        {
            this.storyInteraction = storyInteraction;
        }

        [Authorize]
        public ActionResult Index()
        {
            var model = storyInteraction.GetAllStories();

            return View(model);
        }

        [Authorize]
        public ActionResult GetMediaType()
        {
            return Json(Enum.GetValues(typeof(MediaType)).Cast<MediaType>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Create(StoryDto storyDto, HttpPostedFileBase file, string url)
        {
            storyInteraction.Save(storyDto, file, url);

            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Edit(long? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var model = storyInteraction.GetStoryById(id.Value);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(StoryDto storyDto, HttpPostedFileBase file, string url)
        {
            storyInteraction.UpdateStory(storyDto, file, url);
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult State(long? id)
        {
            storyInteraction.SetState(id);
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult SetOrder(long? id, int order)
        {
            storyInteraction.SetOrder(id, order);
            return RedirectToAction("Index");
        }
    }
}