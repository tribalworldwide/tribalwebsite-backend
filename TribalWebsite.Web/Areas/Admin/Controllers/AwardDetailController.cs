﻿using System.Web.Mvc;
using TribalWebsite.Api.Dto;
using TribalWebsite.Api.Interactions.AwardDetailInteractions;
using TribalWebsite.Api.Interactions.AwardInteractions;
using TribalWebsite.Api.Interactions.WorkInteractions;

namespace TribalWebsite.Web.Areas.Admin.Controllers
{
    public class AwardDetailController : Controller
    {
        private IAwardDetailInteraction awardDetailInteraction;
        private IWorkInteraction workInteraction;
        private IAwardInteraction awardInteraction;

        public AwardDetailController(IAwardDetailInteraction awardDetailInteraction, IAwardInteraction awardInteraction, IWorkInteraction workInteraction)
        {
            this.awardDetailInteraction = awardDetailInteraction;
            this.awardInteraction = awardInteraction;
            this.workInteraction = workInteraction;
        }

        [Authorize]
        public ActionResult Index(long id)
        {
            var model = awardDetailInteraction.GetAwardDetailsByAwardId(id);
            return View(model);
        }

        [Authorize]
        public ActionResult Create()
        {
            ViewBag.Awards = awardInteraction.GetAllAwards();

            ViewBag.Works = workInteraction.GetAllWorksWithoutLazyLoad();

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(AwardDetailDto awardDetailDto)
        {
            var awardDetail = awardDetailInteraction.SaveAwardDetail(awardDetailDto);

            return RedirectToAction("Index", new { id = awardDetail.AwardId });
        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return RedirectToAction("Index", "Awards");

            var model = awardDetailInteraction.GetAwardDetailById(id.Value);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(AwardDetailDto awardDetailDto)
        {
            awardDetailInteraction.UpdateAwardDetail(awardDetailDto);

            return RedirectToAction("Index", new { id = awardDetailDto.AwardId });
        }

        [Authorize]
        public ActionResult Delete(long? id)
        {
            if (id == null)
                return RedirectToAction("Index", "Awards");

            var model = awardDetailInteraction.GetAwardDetailById(id.Value);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(AwardDetailDto awardDetailDto)
        {
            var awardId = awardDetailDto.AwardId;

            awardDetailInteraction.DeleteAwardDetail(awardDetailDto);

            return RedirectToAction("Index", new { id = awardId });
        }

        [Authorize]
        [HttpPost]
        public ActionResult SetOrder(long? id, int order)
        {
            awardDetailInteraction.SetOrder(id, order);

            return RedirectToAction("Index");
        }
    }
}