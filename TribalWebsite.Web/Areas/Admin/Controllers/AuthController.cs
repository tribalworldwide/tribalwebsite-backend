﻿using Infrastructure.Data.Enums;
using Infrastructure.Services.Authentication;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TribalWebsite.Web.Areas.Admin.Controllers
{
    public class AuthController : Controller
    {
        private IAuthenticationService authenticationService;

        public AuthController(IAuthenticationService authenticationService)
        {
            this.authenticationService = authenticationService;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string account, string password)
        {
            var username = string.Empty;
            if (!authenticationService.Authenticate(account, password, out username))
                return RedirectToAction("Login");

            var authTicket = new FormsAuthenticationTicket(1, "LoginTicket", DateTime.Now, DateTime.Now.AddMinutes(30), true, username);
            var cookieContents = FormsAuthentication.Encrypt(authTicket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookieContents)
            {
                Expires = authTicket.Expiration,
                Path = FormsAuthentication.FormsCookiePath,
                HttpOnly = true
            };

            Response.Cookies.Add(cookie);
            Session["Language"] = (int)Languages.Tr;
            return RedirectToAction("Index", "Dashboard");
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login");
        }

    }
}