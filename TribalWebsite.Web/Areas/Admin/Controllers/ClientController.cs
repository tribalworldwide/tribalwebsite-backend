﻿using System.Web;
using System.Web.Mvc;
using TribalWebsite.Api.Dto;
using TribalWebsite.Api.Interactions.ClientInteractions;

namespace TribalWebsite.Web.Areas.Admin.Controllers
{
    public class ClientController : Controller
    {
        private IClientInteraction clientInteraction;

        public ClientController(IClientInteraction clientInteraction)
        {
            this.clientInteraction = clientInteraction;
        }

        [Authorize]
        public ActionResult Index()
        {
            var model = clientInteraction.GetAllClients();

            return View(model);
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(ClientDto clientDto, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                clientInteraction.SaveClient(clientDto, file);

            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Edit(long? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var model = clientInteraction.GetClientById(id.Value);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(ClientDto clientDto, HttpPostedFileBase file)
        {
            clientInteraction.UpdateClient(clientDto, file);
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult State(long? id)
        {
            clientInteraction.SetState(id);
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult SetOrder(long? id, int order)
        {
            clientInteraction.SetOrder(id, order);
            return RedirectToAction("Index");
        }
    }
}