﻿using System.Web.Mvc;

namespace TribalWebsite.Web.Areas.Admin.Controllers
{
    public class DashboardController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
    }
}