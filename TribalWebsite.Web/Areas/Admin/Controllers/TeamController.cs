﻿using System.Web;
using System.Web.Mvc;
using TribalWebsite.Api.Dto;
using TribalWebsite.Api.Interactions.TeamInteractions;

namespace TribalWebsite.Web.Areas.Admin.Controllers
{
    public class TeamController : Controller
    {
        private ITeamInteraction teamInteraction;

        public TeamController(ITeamInteraction teamInteraction)
        {
            this.teamInteraction = teamInteraction;
        }

        [Authorize]
        public ActionResult Index()
        {
            var model = teamInteraction.GetTeam();

            return View(model);
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(TeamDto teamDto, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                teamInteraction.SaveTeam(teamDto, file);

            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var model = teamInteraction.GetTeamById(id.Value);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(TeamDto teamDto, HttpPostedFileBase file)
        {
            teamInteraction.UpdateTeam(teamDto, file);
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Delete(long? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var model = teamInteraction.GetTeamById(id.Value);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(TeamDto teamDto)
        {
            teamInteraction.DeleteTeam(teamDto);
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult SetOrder(long? id, int order)
        {
            teamInteraction.SetOrder(id, order);
            return RedirectToAction("Index");
        }
    }
}