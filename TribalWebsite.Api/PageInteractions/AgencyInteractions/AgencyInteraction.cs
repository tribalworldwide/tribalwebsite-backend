﻿using Infrastructure.Mapping;
using System.Linq;
using TribalWebsite.Api.Dto;
using TribalWebsite.Api.Dto.Page;
using TribalWebsite.Core.Services.AgencyServices;
using TribalWebsite.Core.Services.AwardServices;
using TribalWebsite.Core.Services.ClientServices;
using TribalWebsite.Core.Services.TeamServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Api.PageInteractions.AgencyInteractions
{
    public class AgencyInteraction : MappingService, IAgencyInteraction
    {
        private IAgencyService agencyService;
        private ITeamService teamService;
        private IClientService clientService;
        private IAwardService awardService;

        public AgencyInteraction(IAgencyService agencyService, ITeamService teamService, IClientService clientService, IAwardService awardService)
        {
            this.agencyService = agencyService;
            this.teamService = teamService;
            this.clientService = clientService;
            this.awardService = awardService;
        }

        public AgencyPage GetAgencyPage()
        {
            var agency = MapObject<Agency, AgencyDto>(agencyService.Get());
            var team = MapCollection<Team, TeamDto>(teamService.Get()).ToList();
            var client = MapCollection<Client, ClientDto>(clientService.Get()).ToList();
            var award = MapCollection<Award, AwardDto>(awardService.Get()).ToList();

            return new AgencyPage
            {
                Agency = agency,
                Team = team,
                Client = client,
                Award = award
            };
        }

    }
}
