﻿using TribalWebsite.Api.Dto.Page;

namespace TribalWebsite.Api.PageInteractions.AgencyInteractions
{
    public interface IAgencyInteraction
    {
        AgencyPage GetAgencyPage();
    }
}