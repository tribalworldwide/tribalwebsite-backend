﻿using Infrastructure.Mapping;
using TribalWebsite.Api.Dto;
using TribalWebsite.Core.Services.ContactServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Api.PageInteractions.ContactInteractions
{
    public class ContactInteraction : MappingService, IContactInteraction
    {
        private IContactService contactService;

        public ContactInteraction(IContactService contactService)
        {
            this.contactService = contactService;
        }

        public ContactDto GetContactPage()
        {
            return MapObject<Contact, ContactDto>(contactService.Get());
        }
    }
}
