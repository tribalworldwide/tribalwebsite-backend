﻿using TribalWebsite.Api.Dto;

namespace TribalWebsite.Api.PageInteractions.ContactInteractions
{
    public interface IContactInteraction
    {
        ContactDto GetContactPage();
    }
}