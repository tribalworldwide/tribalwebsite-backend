﻿using System.ComponentModel.DataAnnotations;

namespace TribalWebsite.Api.Dto
{
    public class SocialMediaDto
    {
        public long Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Link { get; set; }

        [Required]
        public string Type { get; set; }
    }
}
