﻿namespace TribalWebsite.Api.Dto
{
    public class TimelineTypeDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}