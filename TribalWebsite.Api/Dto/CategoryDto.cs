﻿using System.ComponentModel.DataAnnotations;
using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class CategoryDto : BaseDto
    {
        [Required]
        public string Name { get; set; }
    }
}