﻿using System.ComponentModel.DataAnnotations;
using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class AwardDetailDto : BaseDto
    {
        [Required]
        public int Year { get; set; }
        [Required]
        [StringLength(750)]
        public string Category { get; set; }
        [Required]
        [StringLength(750)]
        public string Prize { get; set; }

        public long? WorkId { get; set; }
        public virtual WorkDto Work { get; set; }

        public long? AwardId { get; set; }
        public virtual AwardDto Award { get; set; }
    }
}