﻿using System.ComponentModel.DataAnnotations;
using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class TeamDto : BaseDto
    {
        [StringLength(100)]
        public string Fullname { get; set; }

        [Required]
        [StringLength(500)]
        public string Position { get; set; }

        [Required]
        public string Content { get; set; }

        public long? MediaId { get; set; }
        public virtual MediaDto Media { get; set; }
    }
}
