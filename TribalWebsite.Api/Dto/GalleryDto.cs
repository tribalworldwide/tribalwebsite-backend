﻿using System.Collections.Generic;
using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class GalleryDto : BaseDto
    {
        public long? WorkId { get; set; }
        public virtual WorkDto Work { get; set; }

        public long? GalleryTypeId { get; set; }
        public virtual GalleryTypeDto GalleryType { get; set; }

        public ICollection<MediaDto> Media { get; set; }
    }
}
