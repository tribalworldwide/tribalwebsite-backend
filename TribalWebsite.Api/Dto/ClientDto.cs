﻿using System.ComponentModel.DataAnnotations;
using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class ClientDto : BaseDto
    {
        [Required]
        [StringLength(350)]
        public string Name { get; set; }

        public long? MediaId { get; set; }
        public virtual MediaDto Media { get; set; }
    }
}
