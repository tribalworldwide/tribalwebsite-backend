﻿using System.ComponentModel.DataAnnotations;
using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class AgencyDto : BaseDto
    {
        [Required]
        [StringLength(150)]
        public string Content { get; set; }

        [Required]
        public string History { get; set; }

        [Required]
        public string Idea { get; set; }

        public long? MediaId { get; set; }
        public virtual MediaDto Media { get; set; }
    }
}
