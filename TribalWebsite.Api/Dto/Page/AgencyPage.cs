﻿using System.Collections.Generic;

namespace TribalWebsite.Api.Dto.Page
{
    public class AgencyPage
    {
        public AgencyDto Agency { get; set; }
        public IList<TeamDto> Team { get; set; }
        public IList<ClientDto> Client { get; set; }
        public IList<AwardDto> Award { get; set; }
    }
}
