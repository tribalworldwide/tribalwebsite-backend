﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class AwardDto : BaseDto
    {
        [Required]
        [StringLength(300)]
        public string Name { get; set; }

        public long? MediaId { get; set; }
        public virtual MediaDto Media { get; set; }

        public ICollection<AwardDetailDto> AwardDetails { get; set; }
    }
}