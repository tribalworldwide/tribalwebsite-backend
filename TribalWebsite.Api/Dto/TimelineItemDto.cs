﻿using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class TimelineItemDto : BaseDto
    {
        public long? ItemId { get; set; }

        public long? TimelineTypeId { get; set; }

        public virtual TimelineTypeDto TimelineType { get; set; }
    }
}
