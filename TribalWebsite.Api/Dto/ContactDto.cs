﻿using System.ComponentModel.DataAnnotations;

namespace TribalWebsite.Api.Dto
{
    public class ContactDto
    {
        [StringLength(1000)]
        public string Address { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(20)]
        public string Telephone { get; set; }

        [StringLength(20)]
        public string Fax { get; set; }

        [StringLength(250)]
        public string EJournal { get; set; }

        [StringLength(250)]
        public string ShareWithUs { get; set; }
    }
}
