﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class WorkDto : BaseDto
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string ShortDescription { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Brief { get; set; }
        [Required]
        public string Idea { get; set; }

        public string HtmlContent { get; set; }

        [Required]
        public bool IsDouble { get; set; }


        public long? ClientId { get; set; }
        public virtual ClientDto Client { get; set; }

        public long? CategoryId { get; set; }
        public virtual CategoryDto Category { get; set; }

        public long? ThumbnailId { get; set; }
        public virtual MediaDto Thumbnail { get; set; }

        public long? DoubleThumbnailId { get; set; }
        public virtual MediaDto DoubleThumbnail { get; set; }

        public ICollection<AwardDetailDto> AwardDetail { get; set; }
        public ICollection<GalleryDto> Gallery { get; set; }
    }
}