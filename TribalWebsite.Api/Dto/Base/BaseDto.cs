﻿using System;

namespace TribalWebsite.Api.Dto.Base
{
    public class BaseDto
    {
        public long Id { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int Order { get; set; }
        public int State { get; set; }
        public int? LanguageId { get; set; }
    }
}
