﻿namespace TribalWebsite.Api.Dto
{
    public class GalleryTypeDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}