﻿using System;
using System.ComponentModel.DataAnnotations;
using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class TwitterDto : BaseDto
    {
        [Required]
        [StringLength(300)]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public string User { get; set; }
        [Required]
        public string UserLink { get; set; }

        public DateTime? PostDate { get; set; }
    }
}
