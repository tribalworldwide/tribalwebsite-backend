﻿using System;
using System.ComponentModel.DataAnnotations;
using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class NewsDto : BaseDto
    {
        [Required]
        [StringLength(350)]
        public string Title { get; set; }

        [Required]
        [StringLength(750)]
        public string Content { get; set; }

        [Required]
        [StringLength(100)]
        public string LinkTitle { get; set; }

        [Required]
        public string Link { get; set; }

        public DateTime? PublishDate { get; set; }
    }
}
