﻿namespace TribalWebsite.Api.Dto
{
    public class ContactMailsDto
    {
        public long Id { get; set; }
        public string Email { get; set; }
    }
}
