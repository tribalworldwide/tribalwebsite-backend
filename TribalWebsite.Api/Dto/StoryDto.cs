﻿using System.ComponentModel.DataAnnotations;
using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class StoryDto : BaseDto
    {
        [Required]
        [StringLength(350)]
        public string Title { get; set; }

        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        public long? MediaId { get; set; }
        public virtual MediaDto Media { get; set; }
    }
}
