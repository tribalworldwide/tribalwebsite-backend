﻿using System.ComponentModel.DataAnnotations;
using TribalWebsite.Api.Dto.Base;

namespace TribalWebsite.Api.Dto
{
    public class MediaDto : BaseDto
    {
        [Required]
        public int MediaType { get; set; }
        [Required]
        public string FilePath { get; set; }

        public long? GalleryId { get; set; }
        public virtual GalleryDto Gallery { get; set; }
    }
}
