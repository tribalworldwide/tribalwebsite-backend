﻿namespace TribalWebsite.Api.Enums
{
    public enum TimelineTypeEnum
    {
        Work = 1,
        News = 2,
        Media = 3,
        Instagram = 4,
        Twitter = 5
    }
}
