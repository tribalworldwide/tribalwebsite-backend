﻿using Autofac;
using System.Linq;
using TribalWebsite.Core.Ioc;

namespace TribalWebsite.Api.Ioc
{
    public class InteractionsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new ServicesModule());

            builder.RegisterAssemblyTypes(System.Reflection.Assembly.Load("TribalWebsite.Api"))
                .Where(t => t.Name.EndsWith("Interaction"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
