﻿using System.Collections.Generic;
using TribalWebsite.Api.Dto;

namespace TribalWebsite.Api.Interactions.CategoryInteractions
{
    public interface ICategoryInteraction
    {
        IList<CategoryDto> GetAllCategories();

        CategoryDto GetCategoryById(long? id);

        CategoryDto SaveCategory(CategoryDto categoryDto);

        bool UpdateCategory(CategoryDto categoryDto);

        void DeleteCategory(CategoryDto categoryDto);

        void SetOrder(long? objectId, int order);

        void SetState(long? objectId);
    }
}