﻿using Infrastructure.Mapping;
using System.Collections.Generic;
using System.Linq;
using TribalWebsite.Api.Dto;
using TribalWebsite.Core.Services.CategoryServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Api.Interactions.CategoryInteractions
{
    public class CategoryInteraction : MappingService, ICategoryInteraction
    {
        private ICategoryService categoryService;

        public CategoryInteraction(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }

        public IList<CategoryDto> GetAllCategories() => MapCollection<Category, CategoryDto>(categoryService.Get()).ToList();

        public CategoryDto GetCategoryById(long? id) => MapObject<Category, CategoryDto>(categoryService.GetById(id));

        public CategoryDto SaveCategory(CategoryDto categoryDto)
        {
            var category = categoryService.Save(MapObject<CategoryDto, Category>(categoryDto));

            return MapObject<Category, CategoryDto>(category);
        }

        public bool UpdateCategory(CategoryDto categoryDto) => categoryService.Update(MapObject<CategoryDto, Category>(categoryDto));

        public void DeleteCategory(CategoryDto categoryDto) => categoryService.Delete(MapObject<CategoryDto, Category>(categoryDto));

        public void SetOrder(long? objectId, int order) => categoryService.SetOrder(objectId, order);

        public void SetState(long? objectId) => categoryService.SetState(objectId);
    }
}
