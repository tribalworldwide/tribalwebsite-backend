﻿using System.Collections.Generic;
using System.Web;
using TribalWebsite.Api.Dto;

namespace TribalWebsite.Api.Interactions.GalleryInteractions
{
    public interface IGalleryInteraction
    {
        IList<MediaDto> GetGalleryMedias(long? galleryId);
        IList<GalleryDto> GetGalleries(long? workId);
        GalleryDto GetGalleryById(long? id);
        GalleryDto SaveGallery(GalleryDto galleryDto);
        void SaveGalleryMedia(long? galleryId, HttpPostedFileBase file = null, string url = "");
        void UpdateGalleryMedia(long? mediaId, HttpPostedFileBase file = null, string url = "");
        bool UpdateGallery(GalleryDto galleryDto);
        void SetState(long? objectId);
        void SetOrder(long? objectId, int order);
    }
}