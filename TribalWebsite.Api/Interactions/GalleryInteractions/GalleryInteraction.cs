﻿using Infrastructure.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TribalWebsite.Api.Dto;
using TribalWebsite.Core.Services.GalleryServices;
using TribalWebsite.Core.Services.MediaServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Api.Interactions.GalleryInteractions
{
    public class GalleryInteraction : MappingService, IGalleryInteraction
    {
        private IGalleryService galleryService;
        private IMediaService mediaService;

        public GalleryInteraction(IGalleryService galleryService, IMediaService mediaService)
        {
            this.galleryService = galleryService;
            this.mediaService = mediaService;
        }

        public IList<MediaDto> GetGalleryMedias(long? galleryId) => MapCollection<Media, MediaDto>(mediaService.GetByGalleryId(galleryId)).ToList();

        public IList<GalleryDto> GetGalleries(long? workId) => MapCollection<Gallery, GalleryDto>(galleryService.Get(workId)).ToList();

        public GalleryDto GetGalleryById(long? id) => MapObject<Gallery, GalleryDto>(galleryService.GetById(id));

        public GalleryDto SaveGallery(GalleryDto galleryDto)
        {
            var gallery = galleryService.Save(MapObject<GalleryDto, Gallery>(galleryDto));

            return MapObject<Gallery, GalleryDto>(gallery);
        }

        public void SaveGalleryMedia(long? galleryId, HttpPostedFileBase file = null, string url = "") => galleryService.SaveMedia(galleryId, file, url);

        public void UpdateGalleryMedia(long? mediaId, HttpPostedFileBase file = null, string url = "") => galleryService.UpdateMedia(mediaId, file, url);

        public bool UpdateGallery(GalleryDto galleryDto) => galleryService.Update(MapObject<GalleryDto, Gallery>(galleryDto));

        public void SetState(long? objectId) => galleryService.SetState(objectId);

        public void SetOrder(long? objectId, int order) => galleryService.SetOrder(objectId, order);
    }
}
