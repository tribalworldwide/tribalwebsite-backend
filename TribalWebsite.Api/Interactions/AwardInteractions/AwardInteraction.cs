﻿using Infrastructure.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TribalWebsite.Api.Dto;
using TribalWebsite.Core.Services.AwardServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Api.Interactions.AwardInteractions
{
    public class AwardInteraction : MappingService, IAwardInteraction
    {
        private IAwardService awardService;

        public AwardInteraction(IAwardService awardService)
        {
            this.awardService = awardService;
        }

        public IList<AwardDto> GetAllAwards() => MapCollection<Award, AwardDto>(awardService.Get()).ToList();

        public AwardDto GetAwardById(long? id) => MapObject<Award, AwardDto>(awardService.GetById(id));

        public AwardDto SaveAward(AwardDto awardDto, HttpPostedFileBase file)
        {
            var award = awardService.Save(MapObject<AwardDto, Award>(awardDto), file);

            return MapObject<Award, AwardDto>(award);
        }

        public bool UpdateAward(AwardDto awardDto, HttpPostedFileBase file) => awardService.Update(MapObject<AwardDto, Award>(awardDto), file);

        public void DeleteAward(AwardDto awardDto) => awardService.Delete(MapObject<AwardDto, Award>(awardDto));

        public void SetOrder(long? objectId, int order) => awardService.SetOrder(objectId, order);
    }
}
