﻿using System.Collections.Generic;
using System.Web;
using TribalWebsite.Api.Dto;

namespace TribalWebsite.Api.Interactions.AwardInteractions
{
    public interface IAwardInteraction
    {
        IList<AwardDto> GetAllAwards();
        AwardDto GetAwardById(long? id);
        AwardDto SaveAward(AwardDto awardDto, HttpPostedFileBase file);
        bool UpdateAward(AwardDto awardDto, HttpPostedFileBase file);
        void DeleteAward(AwardDto awardDto);
        void SetOrder(long? objectId, int order);
    }
}