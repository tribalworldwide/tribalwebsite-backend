﻿using Infrastructure.Mapping;
using System.Web;
using TribalWebsite.Api.Dto;
using TribalWebsite.Core.Services.AgencyServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Api.Interactions.AgencyInteractions
{
    public class AgencyInteraction : MappingService, IAgencyInteraction
    {
        private IAgencyService agencyService;

        public AgencyInteraction(IAgencyService agencyService)
        {
            this.agencyService = agencyService;
        }

        public AgencyDto GetAgency()
        {
            return MapObject<Agency, AgencyDto>(agencyService.Get());
        }

        public AgencyDto GetAgencyById(long? id)
        {
            return MapObject<Agency, AgencyDto>(agencyService.GetById(id));
        }

        public bool UpdateAgency(AgencyDto agencyDto, HttpPostedFileBase file)
        {
            return agencyService.Update(MapObject<AgencyDto, Agency>(agencyDto), file);
        }
    }
}
