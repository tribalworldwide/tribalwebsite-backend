﻿using System.Web;
using TribalWebsite.Api.Dto;

namespace TribalWebsite.Api.Interactions.AgencyInteractions
{
    public interface IAgencyInteraction
    {
        AgencyDto GetAgency();
        AgencyDto GetAgencyById(long? id);
        bool UpdateAgency(AgencyDto agencyDto, HttpPostedFileBase file);
    }
}