﻿using System.Collections.Generic;
using TribalWebsite.Api.Dto;

namespace TribalWebsite.Api.Interactions.AwardDetailInteractions
{
    public interface IAwardDetailInteraction
    {
        IList<AwardDetailDto> GetAwardDetailsByAwardId(long id);
        AwardDetailDto GetAwardDetailById(long? id);
        AwardDetailDto SaveAwardDetail(AwardDetailDto awardDetailDto);
        bool UpdateAwardDetail(AwardDetailDto awardDetailDto);
        void DeleteAwardDetail(AwardDetailDto awardDetailDto);
        void SetOrder(long? objectId, int order);
    }
}