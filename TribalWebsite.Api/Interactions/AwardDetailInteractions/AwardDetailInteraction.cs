﻿using Infrastructure.Mapping;
using System.Collections.Generic;
using System.Linq;
using TribalWebsite.Api.Dto;
using TribalWebsite.Core.Services.AwardDetailServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Api.Interactions.AwardDetailInteractions
{
    public class AwardDetailInteraction : MappingService, IAwardDetailInteraction
    {
        private IAwardDetailService awardDetailService;

        public AwardDetailInteraction(IAwardDetailService awardDetailService)
        {
            this.awardDetailService = awardDetailService;
        }

        public IList<AwardDetailDto> GetAwardDetailsByAwardId(long id) => MapCollection<AwardDetail, AwardDetailDto>(awardDetailService.GetByAwardId(id)).ToList();

        public AwardDetailDto GetAwardDetailById(long? id) => MapObject<AwardDetail, AwardDetailDto>(awardDetailService.GetById(id));

        public AwardDetailDto SaveAwardDetail(AwardDetailDto awardDetailDto)
        {
            var awardDetail = awardDetailService.Save(MapObject<AwardDetailDto, AwardDetail>(awardDetailDto));

            return MapObject<AwardDetail, AwardDetailDto>(awardDetail);
        }

        public bool UpdateAwardDetail(AwardDetailDto awardDetailDto) => awardDetailService.Update(MapObject<AwardDetailDto, AwardDetail>(awardDetailDto));

        public void DeleteAwardDetail(AwardDetailDto awardDetailDto) => awardDetailService.Delete(MapObject<AwardDetailDto, AwardDetail>(awardDetailDto));

        public void SetOrder(long? objectId, int order) => awardDetailService.SetOrder(objectId, order);

    }
}
