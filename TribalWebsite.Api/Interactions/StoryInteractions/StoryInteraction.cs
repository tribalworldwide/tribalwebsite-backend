﻿using Infrastructure.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TribalWebsite.Api.Dto;
using TribalWebsite.Core.Services.StoryServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Api.Interactions.StoryInteractions
{
    public class StoryInteraction : MappingService, IStoryInteraction
    {
        private IStoryService storyService;

        public StoryInteraction(IStoryService storyService)
        {
            this.storyService = storyService;
        }

        public IList<StoryDto> GetAllStories() => MapCollection<Story, StoryDto>(storyService.Get()).ToList();

        public StoryDto GetStoryById(long? id) => MapObject<Story, StoryDto>(storyService.GetById(id));

        public StoryDto Save(StoryDto storyDto, HttpPostedFileBase file = null, string url = "")
        {
            var story = storyService.Save(MapObject<StoryDto, Story>(storyDto), file, url);

            return MapObject<Story, StoryDto>(story);
        }

        public bool UpdateStory(StoryDto storyDto, HttpPostedFileBase file = null, string url = "") => storyService.Update(MapObject<StoryDto, Story>(storyDto), file, url);

        public void SetState(long? objectId) => storyService.SetState(objectId);

        public void SetOrder(long? objectId, int order) => storyService.SetOrder(objectId, order);
    }
}
