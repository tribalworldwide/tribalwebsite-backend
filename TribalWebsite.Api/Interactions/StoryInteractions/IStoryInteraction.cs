﻿using System.Collections.Generic;
using System.Web;
using TribalWebsite.Api.Dto;

namespace TribalWebsite.Api.Interactions.StoryInteractions
{
    public interface IStoryInteraction
    {
        IList<StoryDto> GetAllStories();
        StoryDto GetStoryById(long? id);
        StoryDto Save(StoryDto storyDto, HttpPostedFileBase file = null, string url = "");
        bool UpdateStory(StoryDto storyDto, HttpPostedFileBase file = null, string url = "");
        void SetState(long? objectId);
        void SetOrder(long? objectId, int order);
    }
}