﻿using System.Collections.Generic;
using System.Web;
using TribalWebsite.Api.Dto;

namespace TribalWebsite.Api.Interactions.TeamInteractions
{
    public interface ITeamInteraction
    {
        IList<TeamDto> GetTeam();

        TeamDto GetTeamById(long? id);

        TeamDto SaveTeam(TeamDto teamDto, HttpPostedFileBase file);

        bool UpdateTeam(TeamDto teamDto, HttpPostedFileBase file);

        void DeleteTeam(TeamDto teamDto);

        void SetOrder(long? objectId, int order);
    }
}