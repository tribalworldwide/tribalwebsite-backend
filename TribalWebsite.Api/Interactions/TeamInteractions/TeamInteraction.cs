﻿using Infrastructure.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TribalWebsite.Api.Dto;
using TribalWebsite.Core.Services.TeamServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Api.Interactions.TeamInteractions
{
    public class TeamInteraction : MappingService, ITeamInteraction
    {
        private ITeamService teamService;

        public TeamInteraction(ITeamService teamService)
        {
            this.teamService = teamService;
        }

        public IList<TeamDto> GetTeam() => MapCollection<Team, TeamDto>(teamService.Get()).ToList();

        public TeamDto GetTeamById(long? id) => MapObject<Team, TeamDto>(teamService.GetById(id));

        public TeamDto SaveTeam(TeamDto teamDto, HttpPostedFileBase file)
        {
            var team = teamService.Save(MapObject<TeamDto, Team>(teamDto), file);

            return MapObject<Team, TeamDto>(team);
        }

        public bool UpdateTeam(TeamDto teamDto, HttpPostedFileBase file) => teamService.Update(MapObject<TeamDto, Team>(teamDto), file);

        public void DeleteTeam(TeamDto teamDto) => teamService.Delete(MapObject<TeamDto, Team>(teamDto));

        public void SetOrder(long? objectId, int order) => teamService.SetOrder(objectId, order);
    }
}
