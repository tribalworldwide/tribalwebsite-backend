﻿using Infrastructure.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TribalWebsite.Api.Dto;
using TribalWebsite.Core.Services.ClientServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Api.Interactions.ClientInteractions
{
    public class ClientInteraction : MappingService, IClientInteraction
    {
        private IClientService clientService;

        public ClientInteraction(IClientService clientService)
        {
            this.clientService = clientService;
        }

        public IList<ClientDto> GetAllClients() => MapCollection<Client, ClientDto>(clientService.Get()).ToList();

        public ClientDto GetClientById(long? id) => MapObject<Client, ClientDto>(clientService.GetById(id));

        public ClientDto SaveClient(ClientDto clientDto, HttpPostedFileBase file)
        {
            var client = clientService.Save(MapObject<ClientDto, Client>(clientDto), file);

            return MapObject<Client, ClientDto>(client);
        }

        public bool UpdateClient(ClientDto clientDto, HttpPostedFileBase file) => clientService.Update(MapObject<ClientDto, Client>(clientDto), file);

        public void SetState(long? objectId) => clientService.SetState(objectId);

        public void SetOrder(long? objectId, int order) => clientService.SetOrder(objectId, order);
    }
}
