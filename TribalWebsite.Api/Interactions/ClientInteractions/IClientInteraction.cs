﻿using System.Collections.Generic;
using System.Web;
using TribalWebsite.Api.Dto;

namespace TribalWebsite.Api.Interactions.ClientInteractions
{
    public interface IClientInteraction
    {
        IList<ClientDto> GetAllClients();

        ClientDto GetClientById(long? id);

        ClientDto SaveClient(ClientDto clientDto, HttpPostedFileBase file);

        bool UpdateClient(ClientDto clientDto, HttpPostedFileBase file);

        void SetState(long? objectId);

        void SetOrder(long? objectId, int order);
    }
}