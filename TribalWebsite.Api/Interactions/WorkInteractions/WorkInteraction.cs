﻿using Infrastructure.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TribalWebsite.Api.Dto;
using TribalWebsite.Core.Services.WorkServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Api.Interactions.WorkInteractions
{
    public class WorkInteraction : MappingService, IWorkInteraction
    {
        private IWorkService workService;

        public WorkInteraction(IWorkService workService)
        {
            this.workService = workService;
        }

        public IList<WorkDto> GetAllWorks() => MapCollection<Work, WorkDto>(workService.Get()).ToList();

        public IList<WorkDto> GetAllWorksWithoutLazyLoad() => MapCollection<Work, WorkDto>(workService.GetWithoutLazyLoad()).ToList();

        public IList<WorkDto> GetWorksWithClientId(long? clientId) => MapCollection<Work, WorkDto>(workService.GetWithClientId(clientId)).ToList();

        public IList<WorkDto> GetWorksWithCategoryId(long? categoryId) => MapCollection<Work, WorkDto>(workService.GetWithCategoryId(categoryId)).ToList();

        public WorkDto GetWorkById(long? id) => MapObject<Work, WorkDto>(workService.GetById(id));

        public WorkDto SaveWork(WorkDto workDto, HttpPostedFileBase thumbFile, HttpPostedFileBase doubleImageFile)
        {
            var work = workService.Save(MapObject<WorkDto, Work>(workDto), thumbFile, doubleImageFile);

            return MapObject<Work, WorkDto>(work);
        }

        public bool UpdateWork(WorkDto workDto, HttpPostedFileBase thumbFile, HttpPostedFileBase doubleImageFile) => workService.Update(MapObject<WorkDto, Work>(workDto), thumbFile, doubleImageFile);

        public void SetState(long? objectId) => workService.SetState(objectId);

        public void SetOrder(long? objectId, int order) => workService.SetOrder(objectId, order);
    }
}
