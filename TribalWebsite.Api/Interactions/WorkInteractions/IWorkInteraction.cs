﻿using System.Collections.Generic;
using System.Web;
using TribalWebsite.Api.Dto;

namespace TribalWebsite.Api.Interactions.WorkInteractions
{
    public interface IWorkInteraction
    {
        IList<WorkDto> GetAllWorks();

        IList<WorkDto> GetAllWorksWithoutLazyLoad();

        IList<WorkDto> GetWorksWithClientId(long? clientId);

        IList<WorkDto> GetWorksWithCategoryId(long? categoryId);

        WorkDto GetWorkById(long? id);

        WorkDto SaveWork(WorkDto workDto, HttpPostedFileBase thumbFile, HttpPostedFileBase doubleImageFile);

        bool UpdateWork(WorkDto workDto, HttpPostedFileBase thumbFile, HttpPostedFileBase doubleImageFile);

        void SetState(long? objectId);

        void SetOrder(long? objectId, int order);
    }
}