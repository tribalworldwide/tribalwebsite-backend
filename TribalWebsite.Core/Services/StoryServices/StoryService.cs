﻿using Infrastructure.Crud;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Helpers;
using Infrastructure.Data.Repositories;
using Infrastructure.Services.LogServices;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TribalWebsite.Core.Services.MediaServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.StoryServices
{
    public class StoryService : CrudService, IStoryService
    {
        private const int ImageWidth = 800;
        private const int ImageHeight = 600;

        private IMediaService mediaService;

        public StoryService(IUnitOfWork unitOfWork, ILogService logService, IMediaService mediaService) : base(unitOfWork, logService)
        {
            this.mediaService = mediaService;
        }

        public IList<Story> Get()
        {
            return Get<Story>(x => x.LanguageId == LanguageHelper.CurrentLanguage, or => or.OrderBy(o => o.Order), "Media").ToList();
        }

        public Story GetById(long? id)
        {
            return Get<Story>(x => x.Id == id.Value, null, "Media").FirstOrDefault();
        }

        public Story Save(Story story, HttpPostedFileBase file = null, string url = "")
        {
            if (file != null && file.ContentLength > 0)
            {
                var media = mediaService.Save(file, ImageWidth, ImageHeight, MediaType.Image);
                story.MediaId = media != null ? media.Id : 0;
            }

            else if (!string.IsNullOrWhiteSpace(url))
            {
                var media = mediaService.SaveVideo(url, MediaType.Video);
                story.MediaId = media != null ? media.Id : 0;
            }

            return Save<Story>(story);
        }

        public bool Update(Story story, HttpPostedFileBase file = null, string url = "")
        {
            if (file != null && file.ContentLength > 0)
            {
                var media = GetById<Media>(story.MediaId);

                mediaService.Update(media, file, ImageWidth, ImageHeight, MediaType.Image);
            }
            else if (!string.IsNullOrWhiteSpace(url))
            {
                var media = GetById<Media>(story.MediaId);

                mediaService.UpdateVideo(media, url, MediaType.Video);
            }

            return Update<Story>(story);
        }

        public void SetState(long? objectId)
        {
            if (objectId.HasValue)
            {
                var story = GetById<Story>(objectId);

                var media = GetById<Media>(story.MediaId);

                if (story.State == (int)State.Active)
                {
                    story.State = (int)State.Passive;

                    media.State = (int)State.Passive;

                    Update(media);
                }
                else
                {
                    story.State = (int)State.Active;

                    media.State = (int)State.Active;

                    Update(media);
                }

                Update<Story>(story);
            }
        }

        public void SetOrder(long? objectId, int order)
        {
            if (objectId.HasValue)
            {
                var story = GetById<Story>(objectId);

                story.Order = order;

                Update<Story>(story);
            }
        }
    }
}
