﻿using System.Collections.Generic;
using System.Web;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.StoryServices
{
    public interface IStoryService
    {
        IList<Story> Get();
        Story GetById(long? id);
        Story Save(Story story, HttpPostedFileBase file = null, string url = "");
        bool Update(Story story, HttpPostedFileBase file = null, string url = "");
        void SetState(long? objectId);
        void SetOrder(long? objectId, int order);
    }
}