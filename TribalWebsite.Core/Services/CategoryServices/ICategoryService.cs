﻿using System.Collections.Generic;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.CategoryServices
{
    public interface ICategoryService
    {
        IList<Category> Get();
        Category GetById(long? id);
        Category Save(Category category);
        bool Update(Category category);
        void Delete(Category category);
        void SetOrder(long? objectId, int order);
        void SetState(long? objectId);
    }
}