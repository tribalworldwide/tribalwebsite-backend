﻿using Infrastructure.Crud;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Helpers;
using Infrastructure.Data.Repositories;
using Infrastructure.Services.LogServices;
using System.Collections.Generic;
using System.Linq;
using TribalWebsite.Core.Services.WorkServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.CategoryServices
{
    public class CategoryService : CrudService, ICategoryService
    {
        private IWorkService workService;

        public CategoryService(IUnitOfWork unitOfWork, ILogService logService, IWorkService workService) : base(unitOfWork, logService)
        {
            this.workService = workService;
        }

        public IList<Category> Get()
        {
            return Get<Category>(x => x.LanguageId == LanguageHelper.CurrentLanguage,
                                      or => or.OrderBy(o => o.Order),
                                      string.Empty).ToList();

        }

        public Category GetById(long? id)
        {
            return Get<Category>(x => x.Id == id.Value).FirstOrDefault();
        }

        public Category Save(Category category)
        {
            return Save<Category>(category);
        }

        public bool Update(Category category)
        {
            return Update<Category>(category);
        }

        public void Delete(Category category)
        {
            foreach (var work in workService.GetWithCategoryId(category.Id))
            {
                work.State = (int)State.Passive;

                Update(work);
            }

            category.State = (int)State.Passive;

            Update<Category>(category);
        }

        public void SetOrder(long? objectId, int order)
        {
            if (objectId.HasValue)
            {
                var category = GetById<Category>(objectId);

                category.Order = order;

                Update(category);
            }
        }

        public void SetState(long? objectId)
        {
            if (objectId.HasValue)
            {
                var category = GetById<Category>(objectId);

                if (category.State == (int)State.Active)
                {
                    category.State = (int)State.Passive;

                    foreach (var work in workService.GetWithCategoryId(objectId))
                    {
                        work.State = (int)State.Passive;

                        Update(work);
                    }
                }
                else
                {
                    category.State = (int)State.Active;

                    foreach (var work in workService.GetWithCategoryId(objectId))
                    {
                        work.State = (int)State.Active;

                        Update(work);
                    }
                }

                Update<Category>(category);
            }
        }


    }
}
