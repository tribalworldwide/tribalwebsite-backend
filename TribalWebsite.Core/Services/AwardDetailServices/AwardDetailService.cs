﻿using Infrastructure.Crud;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Helpers;
using Infrastructure.Data.Repositories;
using Infrastructure.Services.LogServices;
using System.Collections.Generic;
using System.Linq;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.AwardDetailServices
{
    public class AwardDetailService : CrudService, IAwardDetailService
    {
        public AwardDetailService(IUnitOfWork unitOfWork, ILogService logService) : base(unitOfWork, logService)
        {
        }

        public IList<AwardDetail> GetByAwardId(long? awardId)
        {
            if (!awardId.HasValue)
                return null;

            return Get<AwardDetail>(x => x.State == (int)State.Active
                                         && x.LanguageId == LanguageHelper.CurrentLanguage
                                         && x.AwardId == awardId.Value,
                                         or => or.OrderBy(o => o.Order), "Award,Work").ToList();

        }

        public IList<AwardDetail> Get() => Get<AwardDetail>(x => x.LanguageId == LanguageHelper.CurrentLanguage,
                                                            or => or.OrderBy(o => o.Order), "Award,Work").ToList();

        public AwardDetail GetById(long? id) => GetById<AwardDetail>(id);

        public AwardDetail Save(AwardDetail awardDetail) => Save<AwardDetail>(awardDetail);

        public bool Update(AwardDetail awardDetail) => Update<AwardDetail>(awardDetail);

        public void Delete(AwardDetail awardDetail)
        {
            awardDetail.State = (int)State.Passive;

            Update<AwardDetail>(awardDetail);
        }

        public void SetOrder(long? objectId, int order)
        {
            if (objectId.HasValue)
            {
                var awardDetail = GetById<AwardDetail>(objectId);

                awardDetail.Order = order;

                Update(awardDetail);
            }

        }
    }
}
