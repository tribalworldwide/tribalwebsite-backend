﻿using System.Collections.Generic;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.AwardDetailServices
{
    public interface IAwardDetailService
    {
        IList<AwardDetail> GetByAwardId(long? awardId);
        IList<AwardDetail> Get();
        AwardDetail GetById(long? id);
        AwardDetail Save(AwardDetail awardDetail);
        bool Update(AwardDetail awardDetail);
        void Delete(AwardDetail awardDetail);
        void SetOrder(long? objectId, int order);
    }
}