﻿using Infrastructure.Crud;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Repositories;
using Infrastructure.Services.ImageOperations;
using Infrastructure.Services.LogServices;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.MediaServices
{
    public class MediaService : CrudService, IMediaService
    {
        private IImageOperationsService imageOpreationsService;

        public MediaService(IUnitOfWork unitOfWork, ILogService logService, IImageOperationsService imageOpreationsService) : base(unitOfWork, logService)
        {
            this.imageOpreationsService = imageOpreationsService;
        }

        public IList<Media> GetByGalleryId(long? galleryId)
        {
            if (!galleryId.HasValue)
                return new List<Media>();

            return Get<Media>(x => x.GalleryId == galleryId.Value).ToList();
        }
        
        public Media Save(HttpPostedFileBase file, int desiredWidth, int desiredHeight, MediaType mediaType, long? galleryId = null)
        {
            var filePath = imageOpreationsService.SaveAndCropImage(file, desiredWidth, desiredHeight);

            return Save(new Media
            {
                MediaType = (int)mediaType,
                FilePath = filePath,
                GalleryId = galleryId
            });
        }

        public Media SaveVideo(string url, MediaType mediaType, long? galleryId = null)
        {
            return Save(new Media
            {
                MediaType = (int)mediaType,
                FilePath = url,
                GalleryId = galleryId
            });
        }

        public bool Update(Media media, HttpPostedFileBase newFile, int desiredWidth, int desiredHeight, MediaType mediaType)
        {
            var previousPath = string.Empty;

            if (media.MediaType == (int)MediaType.Image)
                previousPath = media.FilePath;

            var filePath = imageOpreationsService.SaveAndCropImage(newFile, desiredWidth, desiredHeight, previousPath: HttpContext.Current.Server.MapPath(previousPath));

            media.FilePath = filePath;

            media.MediaType = (int)mediaType;

            return Update(media);
        }

        public bool UpdateVideo(Media media, string newUrl, MediaType mediaType)
        {
            if (File.Exists(HttpContext.Current.Server.MapPath(media.FilePath)))
                File.Delete(HttpContext.Current.Server.MapPath(media.FilePath));

            media.FilePath = newUrl;
            media.MediaType = (int)mediaType;

            return Update(media);
        }

        public void DeleteFile(string path)
        {
            if (File.Exists(HttpContext.Current.Server.MapPath(path)))
                File.Delete(HttpContext.Current.Server.MapPath(path));
        }
    }
}
