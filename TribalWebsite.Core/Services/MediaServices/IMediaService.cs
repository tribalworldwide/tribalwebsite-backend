﻿using Infrastructure.Data.Enums;
using System.Collections.Generic;
using System.Web;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.MediaServices
{
    public interface IMediaService
    {
        IList<Media> GetByGalleryId(long? galleryId);
        Media Save(HttpPostedFileBase file, int desiredWidth, int desiredHeight, MediaType mediaType, long? galleryId = null);
        Media SaveVideo(string url, MediaType mediaType, long? galleryId = null);
        bool Update(Media media, HttpPostedFileBase newFile, int desiredWidth, int desiredHeight, MediaType mediaType);
        bool UpdateVideo(Media media, string newUrl, MediaType mediaType);
        void DeleteFile(string path);
    }
}