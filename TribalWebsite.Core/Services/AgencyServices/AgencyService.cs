﻿using Infrastructure.Crud;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Helpers;
using Infrastructure.Data.Repositories;
using Infrastructure.Services.LogServices;
using System.Linq;
using System.Web;
using TribalWebsite.Core.Services.MediaServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.AgencyServices
{
    public class AgencyService : CrudService, IAgencyService
    {
        private const int ImageWidth = 1600;
        private const int ImageHeight = 800;

        private IMediaService mediaService;

        public AgencyService(IUnitOfWork unitOfWork, ILogService logService, IMediaService mediaService) : base(unitOfWork, logService)
        {
            this.mediaService = mediaService;
        }

        public Agency Get()
        {
            return Get<Agency>(x => x.State == (int)State.Active && x.LanguageId == LanguageHelper.CurrentLanguage, or => or.OrderBy(o => o.Order), "Media").FirstOrDefault();
        }

        public Agency GetById(long? id)
        {
            return Get<Agency>(x => x.Id == id.Value, null, "Media").FirstOrDefault();
        }

        public bool Update(Agency agency, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                var media = GetById<Media>(agency.MediaId);

                var path = media.FilePath;

                mediaService.Update(media, file, ImageWidth, ImageHeight, MediaType.Image);
            }

            return Update(agency);
        }
    }
}
