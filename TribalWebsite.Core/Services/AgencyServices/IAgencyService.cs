﻿using System.Web;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.AgencyServices
{
    public interface IAgencyService
    {
        Agency Get();
        Agency GetById(long? id);
        bool Update(Agency agency, HttpPostedFileBase file);
    }
}