﻿using System.Collections.Generic;
using System.Web;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.ClientServices
{
    public interface IClientService
    {
        IList<Client> Get();
        Client GetById(long? id);
        Client Save(Client client, HttpPostedFileBase file);
        bool Update(Client client, HttpPostedFileBase file);
        void SetState(long? objectId);
        void SetOrder(long? objectId, int order);
    }
}