﻿using Infrastructure.Crud;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Helpers;
using Infrastructure.Data.Repositories;
using Infrastructure.Services.LogServices;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TribalWebsite.Core.Services.MediaServices;
using TribalWebsite.Core.Services.WorkServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.ClientServices
{
    public class ClientService : CrudService, IClientService
    {
        private const int ImageWidth = 800;
        private const int ImageHeight = 600;

        private IMediaService mediaService;
        private IWorkService workService;

        public ClientService(IUnitOfWork unitOfWork, ILogService logService, IMediaService mediaService, IWorkService workService) : base(unitOfWork, logService)
        {
            this.mediaService = mediaService;
            this.workService = workService;
        }

        public IList<Client> Get()
        {
            return Get<Client>(x => x.LanguageId == LanguageHelper.CurrentLanguage, or => or.OrderBy(o => o.Order), "Media").ToList();
        }

        public Client GetById(long? id)
        {
            return Get<Client>(x => x.Id == id.Value, null, "Media").FirstOrDefault();
        }

        public Client Save(Client client, HttpPostedFileBase file)
        {
            var media = mediaService.Save(file, ImageWidth, ImageHeight, MediaType.Image);

            client.MediaId = media != null ? media.Id : 0;

            return Save(client);

        }

        public bool Update(Client client, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                var media = GetById<Media>(client.MediaId);

                mediaService.Update(media, file, ImageWidth, ImageHeight, MediaType.Image);
            }

            return Update(client);
        }


        public void SetOrder(long? objectId, int order)
        {
            if (objectId.HasValue)
            {
                var client = GetById<Client>(objectId);

                client.Order = order;

                Update(client);
            }
        }

        public void SetState(long? objectId)
        {
            if (objectId.HasValue)
            {
                var client = GetById<Client>(objectId);

                var media = GetById<Media>(client.MediaId);

                if (client.State == (int)State.Active)
                {
                    client.State = (int)State.Passive;

                    foreach (var work in workService.GetWithClientId(objectId))
                    {
                        work.State = (int)State.Passive;

                        Update(work);
                    }

                    media.State = (int)State.Passive;
                }
                else
                {
                    client.State = (int)State.Active;

                    foreach (var work in workService.GetWithClientId(objectId))
                    {
                        work.State = (int)State.Active;

                        Update(work);
                    }

                    media.State = (int)State.Active;
                }

                Update(client);
            }
        }

    }
}
