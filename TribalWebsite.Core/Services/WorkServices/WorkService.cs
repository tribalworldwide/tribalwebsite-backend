﻿using Infrastructure.Crud;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Helpers;
using Infrastructure.Data.Repositories;
using Infrastructure.Services.LogServices;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TribalWebsite.Core.Services.GalleryServices;
using TribalWebsite.Core.Services.MediaServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.WorkServices
{
    public class WorkService : CrudService, IWorkService
    {
        private const int ThumbWidth = 800;
        private const int ThumbHeight = 600;

        private const int DoubleImageWidth = 1600;
        private const int DoubleImageHeight = 800;

        private IMediaService mediaService;
        private IGalleryService galleryService;

        public WorkService(IUnitOfWork unitOfWork, ILogService logService, IMediaService mediaService, IGalleryService galleryService) : base(unitOfWork, logService)
        {
            this.mediaService = mediaService;
            this.galleryService = galleryService;
        }

        public IList<Work> Get()
        {
            return Get<Work>(x => x.LanguageId == LanguageHelper.CurrentLanguage, or => or.OrderBy(o => o.Order), "Client,Category,Thumbnail,DoubleThumbnail").ToList();
        }

        public IList<Work> GetWithoutLazyLoad()
        {
            return Get<Work>(x => x.LanguageId == LanguageHelper.CurrentLanguage, or => or.OrderBy(o => o.Order), "Thumbnail").ToList();
        }

        public IList<Work> GetWithClientId(long? clientId)
        {
            if (!clientId.HasValue)
                return new List<Work>();

            return Get<Work>(x => x.ClientId == clientId.Value
                                  && x.State == (int)State.Active
                                  && x.LanguageId == LanguageHelper.CurrentLanguage,
                                  null,
                                  "Client,Category,Thumbnail,DoubleThumbnail")
                                  .ToList();
        }

        public IList<Work> GetWithCategoryId(long? categoryId)
        {
            if (!categoryId.HasValue)
                return new List<Work>();

            return Get<Work>(x => x.CategoryId == categoryId.Value
                                  && x.State == (int)State.Active
                                  && x.LanguageId == LanguageHelper.CurrentLanguage,
                                  null,
                                  "Client,Category,Thumbnail,DoubleThumbnail")
                                  .ToList();
        }

        public Work GetById(long? id)
        {
            return Get<Work>(x => x.Id == id.Value, null, "Client,Category,Thumbnail,DoubleThumbnail").FirstOrDefault();
        }

        public Work Save(Work work, HttpPostedFileBase thumbFile, HttpPostedFileBase doubleThumbFile)
        {
            var media = new Media();

            #region Thumbnail

            media = mediaService.Save(thumbFile, ThumbWidth, ThumbHeight, MediaType.Image);

            work.ThumbnailId = media.Id;

            work.Thumbnail = media;

            #endregion

            #region DoubleThumbnail

            if (doubleThumbFile != null && doubleThumbFile.ContentLength > 0)
            {
                media = mediaService.Save(doubleThumbFile, DoubleImageWidth, DoubleImageHeight, MediaType.Image);

                work.DoubleThumbnailId = media.Id;

                work.DoubleThumbnail = media;

                work.IsDouble = true;
            }

            #endregion

            return Save(work);
        }

        public bool Update(Work work, HttpPostedFileBase thumbFile, HttpPostedFileBase doubleImageFile)
        {
            if (thumbFile != null && thumbFile.ContentLength > 0)
            {
                var media = GetById<Media>(work.ThumbnailId);

                mediaService.Update(media, thumbFile, ThumbWidth, ThumbHeight, MediaType.Image);
            }

            if (doubleImageFile != null && doubleImageFile.ContentLength > 0)
            {
                var media = GetById<Media>(work.DoubleThumbnailId);

                if (media != null)
                    mediaService.Update(media, doubleImageFile, DoubleImageWidth, DoubleImageHeight, MediaType.Image);
                else
                {
                    var newMedia = mediaService.Save(doubleImageFile, DoubleImageWidth, DoubleImageHeight, MediaType.Image);

                    work.DoubleThumbnailId = newMedia.Id;

                    work.DoubleThumbnail = newMedia;
                }

                work.IsDouble = true;
            }

            return Update(work);
        }

        public void SetState(long? objectId)
        {
            if (objectId.HasValue)
            {
                var work = GetById<Work>(objectId);

                var thumbnail = GetById<Media>(work.ThumbnailId);

                var doubleThumbnail = GetById<Media>(work.DoubleThumbnailId);

                if (work.State == (int)State.Active)
                {
                    foreach(var gallery in galleryService.Get(objectId.Value))
                    {
                        gallery.State = (int)State.Passive;
                        Update(gallery);
                    }

                    work.State = (int)State.Passive;

                    thumbnail.State = (int)State.Passive;

                    doubleThumbnail.State = (int)State.Passive;

                    Update(thumbnail);

                    Update(doubleThumbnail);
                }
                else
                {
                    foreach (var gallery in galleryService.Get(objectId.Value))
                    {
                        gallery.State = (int)State.Active;
                        Update(gallery);
                    }

                    work.State = (int)State.Active;

                    thumbnail.State = (int)State.Active;

                    doubleThumbnail.State = (int)State.Active;

                    Update(thumbnail);

                    Update(doubleThumbnail);
                }

                Update(work);
            }
        }

        public void SetOrder(long? objectId, int order)
        {
            if (objectId.HasValue)
            {
                var work = GetById<Work>(objectId);

                work.Order = order;

                Update(work);
            }

        }

    }
}
