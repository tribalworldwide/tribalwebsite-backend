﻿using System.Collections.Generic;
using System.Web;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.WorkServices
{
    public interface IWorkService
    {
        IList<Work> Get();

        IList<Work> GetWithoutLazyLoad();

        IList<Work> GetWithClientId(long? clientId);

        IList<Work> GetWithCategoryId(long? categoryId);

        Work GetById(long? id);

        Work Save(Work work, HttpPostedFileBase thumbFile, HttpPostedFileBase doubleThumbFile);

        bool Update(Work work, HttpPostedFileBase thumbFile, HttpPostedFileBase doubleImageFile);

        void SetState(long? objectId);

        void SetOrder(long? objectId, int order);
    }
}