﻿using Infrastructure.Crud;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Helpers;
using Infrastructure.Data.Repositories;
using Infrastructure.Services.LogServices;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TribalWebsite.Core.Services.MediaServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.TeamServices
{
    public class TeamService : CrudService, ITeamService
    {
        private const int ImageWidth = 800;
        private const int ImageHeight = 600;

        private IMediaService mediaService;

        public TeamService(IUnitOfWork unitOfWork, ILogService logService, IMediaService mediaService) : base(unitOfWork, logService)
        {
            this.mediaService = mediaService;
        }

        public IList<Team> Get()
        {
            return Get<Team>(x => x.LanguageId == LanguageHelper.CurrentLanguage, or => or.OrderBy(o => o.Order), "Media").ToList();
        }

        public Team GetById(long? id)
        {
            return Get<Team>(x => x.Id == id.Value, null, "Media").FirstOrDefault();
        }

        public Team Save(Team team, HttpPostedFileBase file)
        {
            var media = mediaService.Save(file, ImageWidth, ImageHeight, MediaType.Image);

            team.MediaId = media != null ? media.Id : 0;

            return Save(team);

        }

        public bool Update(Team team, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                var media = GetById<Media>(team.MediaId);

                mediaService.Update(media, file, ImageWidth, ImageHeight, MediaType.Image);
            }

            return Update(team);
        }

        public void Delete(Team team)
        {
            var media = GetById<Media>(team.MediaId);

            media.State = (int)State.Passive;

            Update(media);

            team.State = (int)State.Passive;

            Update(team);
        }

        public void SetOrder(long? objectId, int order)
        {
            if (objectId.HasValue)
            {
                var team = GetById<Team>(objectId);

                team.Order = order;

                Update(team);
            }
        }
    }
}
