﻿using System.Collections.Generic;
using System.Web;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.TeamServices
{
    public interface ITeamService
    {
        IList<Team> Get();
        Team GetById(long? id);
        Team Save(Team team, HttpPostedFileBase file);
        bool Update(Team team, HttpPostedFileBase file);
        void Delete(Team team);
        void SetOrder(long? objectId, int order);
    }
}