﻿using Infrastructure.Crud;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Helpers;
using Infrastructure.Data.Repositories;
using Infrastructure.Services.LogServices;
using System.Linq;
using TribalWebsite.Core.Services.MediaServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.ContactServices
{
    public class ContactService : CrudService, IContactService
    {
        private MediaServices.IMediaService mediaService;

        public ContactService(IUnitOfWork unitOfWork, ILogService logService, IMediaService mediaService) : base(unitOfWork, logService)
        {
            this.mediaService = mediaService;
        }

        public Contact Get()
        {
            return Get<Contact>(x => x.LanguageId == LanguageHelper.CurrentLanguage && x.State == (int)State.Active).FirstOrDefault();
        }
    }
}
