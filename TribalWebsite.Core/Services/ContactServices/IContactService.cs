﻿using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.ContactServices
{
    public interface IContactService
    {
        Contact Get();
    }
}