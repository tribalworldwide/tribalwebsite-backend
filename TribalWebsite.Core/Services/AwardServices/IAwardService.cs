﻿using System.Collections.Generic;
using System.Web;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.AwardServices
{
    public interface IAwardService
    {
        IList<Award> Get();
        Award GetById(long? id);
        Award Save(Award award, HttpPostedFileBase file);
        bool Update(Award award, HttpPostedFileBase file);
        void Delete(Award award);
        void SetOrder(long? objectId, int order);
    }
}