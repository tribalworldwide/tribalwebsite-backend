﻿using Infrastructure.Crud;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Helpers;
using Infrastructure.Data.Repositories;
using Infrastructure.Services.LogServices;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TribalWebsite.Core.Services.AwardDetailServices;
using TribalWebsite.Core.Services.MediaServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.AwardServices
{
    public class AwardService : CrudService, IAwardService
    {
        private const int ImageWidth = 800;
        private const int ImageHeight = 600;

        private IMediaService mediaService;
        private IAwardDetailService awardDetailService;

        public AwardService(IUnitOfWork unitOfWork, ILogService logService, IMediaService mediaService, IAwardDetailService awardDetailService) : base(unitOfWork, logService)
        {
            this.mediaService = mediaService;
            this.awardDetailService = awardDetailService;
        }

        public IList<Award> Get()
        {
            return Get<Award>(x => x.LanguageId == LanguageHelper.CurrentLanguage, or => or.OrderBy(o => o.Order), "Media").ToList();
        }

        public Award GetById(long? id)
        {
            return Get<Award>(x => x.Id == id.Value, null, "Media").FirstOrDefault();
        }

        public Award Save(Award award, HttpPostedFileBase file)
        {
            var media = mediaService.Save(file, ImageWidth, ImageHeight, MediaType.Image);

            award.MediaId = media != null ? media.Id : 0;

            return Save(award);

        }

        public bool Update(Award award, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                var media = GetById<Media>(award.MediaId);

                mediaService.Update(media, file, ImageWidth, ImageHeight, MediaType.Image);
            }

            return Update(award);
        }

        public void Delete(Award award)
        {
            foreach (var awardDetail in awardDetailService.GetByAwardId(award.Id))
            {
                awardDetailService.Delete(awardDetail);
            }

            var media = GetById<Media>(award.MediaId);

            media.State = (int)State.Passive;

            Update(media);

            award.State = (int)State.Passive;

            Update(award);

           
        }

        public void SetOrder(long? objectId, int order)
        {
            if (objectId.HasValue)
            {
                var award = GetById<Award>(objectId);

                award.Order = order;

                Update(award);
            }

        }
    }
}
