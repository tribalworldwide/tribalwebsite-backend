﻿using System.Collections.Generic;
using System.Web;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.GalleryServices
{
    public interface IGalleryService
    {
        IList<Gallery> Get(long? workId);
        Gallery GetById(long? id);
        Gallery Save(Gallery gallery);
        void SaveMedia(long? galleryId, HttpPostedFileBase file = null, string url = "");
        void UpdateMedia(long? mediaId, HttpPostedFileBase file = null, string url = "");
        bool Update(Gallery gallery);
        void SetOrder(long? objectId, int order);
        void SetState(long? objectId);
    }
}