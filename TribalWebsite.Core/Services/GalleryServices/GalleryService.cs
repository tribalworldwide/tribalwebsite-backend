﻿using Infrastructure.Crud;
using Infrastructure.Data.Enums;
using Infrastructure.Data.Helpers;
using Infrastructure.Data.Repositories;
using Infrastructure.Services.LogServices;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TribalWebsite.Core.Services.MediaServices;
using TribalWebsite.Core.Services.WorkServices;
using TribalWebsite.Data.DbStructure.Entities;

namespace TribalWebsite.Core.Services.GalleryServices
{
    public class GalleryService : CrudService, IGalleryService
    {
        private const int ImageWidth = 800;
        private const int ImageHeight = 600;

        private IMediaService mediaService;
        private IWorkService workService;

        public GalleryService(IUnitOfWork unitOfWork, ILogService logService, IMediaService mediaService, IWorkService workService) : base(unitOfWork, logService)
        {
            this.mediaService = mediaService;
            this.workService = workService;
        }

        public IList<Gallery> Get(long? workId)
        {
            if (!workId.HasValue)
                return new List<Gallery>();

            return Get<Gallery>(x => x.WorkId == workId.Value && x.LanguageId == LanguageHelper.CurrentLanguage, or => or.OrderBy(o => o.Order)).ToList();
        }

        public Gallery GetById(long? id)
        {
            return Get<Gallery>(x => x.Id == id.Value).FirstOrDefault();
        }

        public Gallery Save(Gallery gallery)
        {
            return Save<Gallery>(gallery);
        }

        public void SaveMedia(long? galleryId, HttpPostedFileBase file = null, string url = "")
        {
            if (file != null && file.ContentLength > 0)
                mediaService.Save(file, ImageWidth, ImageHeight, MediaType.Image, galleryId);

            else if (!string.IsNullOrWhiteSpace(url))
               mediaService.SaveVideo(url, MediaType.Video, galleryId);
        }

        public void UpdateMedia(long? mediaId, HttpPostedFileBase file = null, string url = "")
        {
            var media = GetById<Media>(mediaId);

            if (file != null && file.ContentLength > 0)
                mediaService.Update(media, file, ImageWidth, ImageHeight, MediaType.Image);

            else if (!string.IsNullOrWhiteSpace(url))
                mediaService.UpdateVideo(media, url, MediaType.Video);
        }

        public bool Update(Gallery gallery)
        {
            return Update<Gallery>(gallery);
        }

        public void SetState(long? objectId)
        {
            var gallery = GetById<Gallery>(objectId);

            gallery.State = (int)State.Passive;

            Update(gallery);

            foreach (var media in mediaService.GetByGalleryId(objectId.Value))
            {
                media.State = (int)State.Passive;

                Update(media);
            }

        }

        public void SetOrder(long? objectId, int order)
        {
            if (objectId.HasValue)
            {
                var gallery = GetById<Gallery>(objectId);

                gallery.Order = order;

                Update(gallery);
            }

        }
    }
}
