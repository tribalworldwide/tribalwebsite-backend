﻿using Autofac;
using Infrastructure.Ioc;
using System.Data.Entity;
using TribalWebsite.Data.DbStructure.Context;

namespace TribalWebsite.Core.Ioc
{
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(TribalWebsiteContext)).As(typeof(DbContext)).InstancePerLifetimeScope();
            builder.RegisterModule(new InfrastructureModule());

            builder.RegisterAssemblyTypes(System.Reflection.Assembly.Load("TribalWebsite.Core"))
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
